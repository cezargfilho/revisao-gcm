package br.ucsal.gcm.revisao.test;

import org.junit.Assert;
import org.junit.Test;

import br.ucsal.gcm.revisao.Calculadora;

public class CalculadoraTeste {

	@Test
	public void testCalcFatorial() {
		int numInput = 5;
		int numEsperado = 120;
		int numAtual = Calculadora.calculoFatorial(numInput);
		Assert.assertEquals(numEsperado, numAtual);

	}

}
